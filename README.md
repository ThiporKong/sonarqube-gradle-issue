# sonarqube-gradle-plugin: Wrong base directory derived for sub-projects two levels deep

Hi,

I have a multi-project with the following structure:

    :very:bestappever
    :bestappever-distrib

The SonarQubePlugin gives

    :sonarqube
    09:35:05.139 INFO  - Load global repositories
    09:35:05.252 INFO  - Load global repositories (done) | time=114ms
    09:35:05.254 INFO  - Server id: 20160428045618
    09:35:05.256 INFO  - User cache: C:\Users\tk09\.sonar\cache
    09:35:05.265 INFO  - Install plugins
    09:35:05.401 INFO  - Install JDBC driver
    09:35:05.414 INFO  - Create JDBC datasource for jdbc:mysql://10.48.213.38:3306/sonar
    09:35:06.456 INFO  - Initializing Hibernate
    :sonarqube FAILED

    FAILURE: Build failed with an exception.

    * What went wrong:
    Execution failed for task ':sonarqube'.
    > The base directory of the module ':very:bestappever' does not exist: C:\Workarea\verybestappever\very\:very:bestappever

You may note, that the full path of project ':very:bestappever' is inserted at the wrong place.

This may be related to https://jira.sonarsource.com/browse/SONARGRADL-12 from which I've adopted the bestappever example by moving the :bestappever project to :very:bestappever. Attached, please find a ZIP file with a minimal example demonstrating this behaviour.

Am I missing something? Are there any configuration options that can be used to work with deeply nested gradle multi-projects? I've tried the proposals from http://stackoverflow.com/questions/35851738/sonarqube-how-to-configure-gradle-sub-projects-correctly but to no avail.

Thanks and regards,

Thipor
